# Crash fix for Battlefield 4

## Download

You can download the file [in the releases section](https://gitlab.com/polloloco/battlefield-4-client-crash-fix/-/releases).
Alternatively, if you do not trust the compiled binaries, you can compile the source code yourself. Instructions are given below.

## Installation

To install the fix, copy the `dinput8.dll` file to your Battlefield 4 game root, usually at `C:\Program Files (x86)\Origin Games\Battlefield 4`.

## About

This project provides a simple fix for one specific bug (only for that one, nothing else) in the Battlefield 4 client that usually ends up crashing your game.

## Will this fix my crashes? I'm using this and I'm still crashing!

If your game is crashing and you want to find out if this would help you, follow these steps:

- Open Event Viewer by pressing `Win + R` and entering `eventvwr.msc`

![event viewer](https://i.imgur.com/EwOE2UR.png)
- Go to `Windows Logs` and then `Application`

![event viewer](https://i.imgur.com/h5Lz0Q8.png)
- Find the Error event for bf4.exe

![event](https://i.imgur.com/OgpgHbl.png)

- Check the `Fault offset`, it has to be `0x000000000023c334`. If its something else, this fix **WILL NOT** work for you

## Compiling

Use the package manager [vcpkg](https://github.com/Microsoft/vcpkg) to install the dependencies.

```bash
vcpkg install minhook spdlog --triplet x64-windows-static
```

The project was compiled using Visual Studio 2019 Enterprise (Version 16.6.3), but any halfway modern compiler for Windows should work.
It has to support C++17 though.

Using Visual Studio, you just have to open the .sln, select `Release` or `Debug` and hit `Build solution`.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)