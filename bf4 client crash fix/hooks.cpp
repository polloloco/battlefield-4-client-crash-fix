#include "hooks.hpp"

#include "sdk.hpp"

#include <Windows.h>
#include <DbgHelp.h>

#include <spdlog/spdlog.h>
#include <fmt/format.h>

#include <array>
#include <string>
#include <string_view>
#include <ctime>
#include <locale>
#include <filesystem>

void hook::function(std::uintptr_t const a1, std::uintptr_t const a2, std::uintptr_t const a3)
{
	for (fb::ClientPlayerLogoEntity* entity : fb::EntityList<fb::ClientPlayerLogoEntity>{}) {
		if (entity->loaded && !entity->buffer) {
			entity->loaded = false;
			MessageBeep(MB_ICONWARNING);

			spdlog::warn("Crash prevented from entity at 0x{:X}", reinterpret_cast<std::uintptr_t>(entity));
		}
	}
	return function_o(a1, a2, a3);
}

static std::filesystem::path executable_path()
{
	HMODULE const hmodule = GetModuleHandleA(NULL);
	std::array<char, MAX_PATH> path{};
	GetModuleFileNameA(hmodule, path.data(), static_cast<DWORD>(path.size()));

	return std::filesystem::path(path.data()).remove_filename();
}

static std::string current_date()
{
	std::time_t const t = std::time(nullptr);
	std::array<char, 0x80> buffer{};

	std::locale::global(std::locale("en_US.UTF8"));
	tm buf{};
	gmtime_s(&buf, &t);
	std::size_t const length = std::strftime(buffer.data(), buffer.size(), "%Y-%m-%d.%H-%M-%S", &buf);

	return { buffer.data(), length };
}

static std::string module_name(std::uintptr_t const address)
{
	HMODULE hmod{};
	std::array<char, MAX_PATH> buf{};
	GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, reinterpret_cast<LPCSTR>(address), &hmod);
	if (hmod) {
		GetModuleFileNameA(hmod, buf.data(), static_cast<DWORD>(buf.size()));
		std::filesystem::path const path{ buf.data() };
		std::string modulename{ path.filename().string() };
		std::transform(std::begin(modulename), std::end(modulename), std::begin(modulename), [](const char c) {
			return static_cast<char>(std::tolower(static_cast<int>(c)));
			});
		return modulename;
	}
	return buf.data();
}

constexpr std::pair<std::string_view, std::string_view> exception_code_to_english(DWORD const code)
{
	using namespace std::literals::string_view_literals;
	switch (code) {
	case EXCEPTION_ACCESS_VIOLATION:
		return { "EXCEPTION_ACCESS_VIOLATION"sv, "The thread tried to read from or write to a virtual address for which it does not have the appropriate access"sv };
	case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
		return { "EXCEPTION_ARRAY_BOUNDS_EXCEEDED"sv, "The thread tried to access an array element that is out of bounds and the underlying hardware supports bounds checking"sv };
	case EXCEPTION_BREAKPOINT:
		return { "EXCEPTION_BREAKPOINT"sv, "A breakpoint was encountered"sv };
	case EXCEPTION_DATATYPE_MISALIGNMENT:
		return { "EXCEPTION_DATATYPE_MISALIGNMENT"sv, "The thread tried to read or write data that is misaligned on hardware that does not provide alignment. For example, 16-bit values must be aligned on 2-byte boundaries; 32-bit values on 4-byte boundaries, and so on"sv };
	case EXCEPTION_FLT_DENORMAL_OPERAND:
		return { "EXCEPTION_FLT_DENORMAL_OPERAND"sv, "One of the operands in a floating-point operation is denormal. A denormal value is one that is too small to represent as a standard floating-point value"sv };
	case EXCEPTION_FLT_DIVIDE_BY_ZERO:
		return { "EXCEPTION_FLT_DIVIDE_BY_ZERO"sv, "The thread tried to divide a floating-point value by a floating-point divisor of zero"sv };
	case EXCEPTION_FLT_INEXACT_RESULT:
		return { "EXCEPTION_FLT_INEXACT_RESULT"sv, "The result of a floating-point operation cannot be represented exactly as a decimal fraction"sv };
	case EXCEPTION_FLT_INVALID_OPERATION:
		return { "EXCEPTION_FLT_INVALID_OPERATION"sv, "This exception represents any floating-point exception not included in this list"sv };
	case EXCEPTION_FLT_OVERFLOW:
		return { "EXCEPTION_FLT_OVERFLOW"sv, "The exponent of a floating-point operation is greater than the magnitude allowed by the corresponding type"sv };
	case EXCEPTION_FLT_STACK_CHECK:
		return { "EXCEPTION_FLT_STACK_CHECK"sv, "The stack overflowed or underflowed as the result of a floating-point operation"sv };
	case EXCEPTION_FLT_UNDERFLOW:
		return { "EXCEPTION_FLT_UNDERFLOW"sv, "The exponent of a floating-point operation is less than the magnitude allowed by the corresponding type"sv };
	case EXCEPTION_ILLEGAL_INSTRUCTION:
		return { "EXCEPTION_ILLEGAL_INSTRUCTION"sv, "The thread tried to execute an invalid instruction"sv };
	case EXCEPTION_IN_PAGE_ERROR:
		return { "EXCEPTION_IN_PAGE_ERROR"sv, "The thread tried to access a page that was not present, and the system was unable to load the page. For example, this exception might occur if a network connection is lost while running a program over the network"sv };
	case EXCEPTION_INT_DIVIDE_BY_ZERO:
		return { "EXCEPTION_INT_DIVIDE_BY_ZERO"sv, "The thread tried to divide an integer value by an integer divisor of zero"sv };
	case EXCEPTION_INT_OVERFLOW:
		return { "EXCEPTION_INT_OVERFLOW"sv, "The result of an integer operation caused a carry out of the most significant bit of the result"sv };
	case EXCEPTION_INVALID_DISPOSITION:
		return { "EXCEPTION_INVALID_DISPOSITION"sv, "An exception handler returned an invalid disposition to the exception dispatcher. Programmers using a high-level language such as C should never encounter this exception"sv };
	case EXCEPTION_NONCONTINUABLE_EXCEPTION:
		return { "EXCEPTION_NONCONTINUABLE_EXCEPTION"sv, "The thread tried to continue execution after a noncontinuable exception occurred"sv };
	case EXCEPTION_PRIV_INSTRUCTION:
		return { "EXCEPTION_PRIV_INSTRUCTION"sv, "The thread tried to execute an instruction whose operation is not allowed in the current machine mode"sv };
	case EXCEPTION_SINGLE_STEP:
		return { "EXCEPTION_SINGLE_STEP"sv, "A trace trap or other single-instruction mechanism signaled that one instruction has been executed"sv };
	case EXCEPTION_STACK_OVERFLOW:
		return { "EXCEPTION_STACK_OVERFLOW"sv, "The thread used up its stack"sv };
	default:
		return { "UNKNOWN"sv, "Unknown exception"sv };
	}
}

void write_dump(PEXCEPTION_POINTERS ExceptionInfo)
{
	MINIDUMP_EXCEPTION_INFORMATION m{};
	m.ExceptionPointers = ExceptionInfo;
	m.ThreadId = GetCurrentThreadId();

	std::string const filename{ fmt::format("minidump_{}.dmp", current_date()) };
	std::filesystem::path const path{ executable_path() / filename };

	if (HANDLE const file = CreateFileA(path.string().c_str(), GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr); file != INVALID_HANDLE_VALUE) {
		MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), file, MiniDumpNormal, &m, nullptr, nullptr);
		CloseHandle(file);
	}
}

LONG hook::unhandled_exception_filter(PEXCEPTION_POINTERS ExceptionInfo)
{
	std::uintptr_t const address_of_exception{ reinterpret_cast<std::uintptr_t>(ExceptionInfo->ExceptionRecord->ExceptionAddress) };
	std::uintptr_t const rva{ [](std::uintptr_t const absolute) {
		MEMORY_BASIC_INFORMATION mbi;
		HMODULE mod{};
		if (VirtualQuery(reinterpret_cast<LPCVOID>(absolute), &mbi, sizeof(mbi))) {
			return absolute - reinterpret_cast<std::uintptr_t>(mbi.AllocationBase);
		}
		else {
			return absolute;
		}
	}(address_of_exception) };
	spdlog::critical("Unhandled exception in module \"{}\" at address 0x{:X} rva 0x{:X}", module_name(address_of_exception), address_of_exception, rva);

	std::string_view const access_type = [](PEXCEPTION_POINTERS ExceptionInfo) {
		using namespace std::literals::string_view_literals;
		if (ExceptionInfo->ExceptionRecord->NumberParameters > 0) {
			const auto type = ExceptionInfo->ExceptionRecord->ExceptionInformation[0];
			switch (type) {
			case 0:
				return "Read"sv;
			case 1:
				return "Write"sv;
			case 8:
				return "DEP"sv;
			}
		}
		return "WTF"sv;
	}(ExceptionInfo);

	std::uintptr_t const access_address = [](PEXCEPTION_POINTERS ExceptionInfo) -> std::uintptr_t {
		if (ExceptionInfo->ExceptionRecord->NumberParameters > 1) {
			return static_cast<std::uintptr_t>(ExceptionInfo->ExceptionRecord->ExceptionInformation[1]);
		}
		return 0;
	}(ExceptionInfo);

	auto const [exception_definition, exception_description] = exception_code_to_english(ExceptionInfo->ExceptionRecord->ExceptionCode);

	switch (ExceptionInfo->ExceptionRecord->ExceptionCode) {
	case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
	case EXCEPTION_BREAKPOINT:
	case EXCEPTION_DATATYPE_MISALIGNMENT:
	case EXCEPTION_FLT_DENORMAL_OPERAND:
	case EXCEPTION_FLT_DIVIDE_BY_ZERO:
	case EXCEPTION_FLT_INEXACT_RESULT:
	case EXCEPTION_FLT_INVALID_OPERATION:
	case EXCEPTION_FLT_OVERFLOW:
	case EXCEPTION_FLT_STACK_CHECK:
	case EXCEPTION_FLT_UNDERFLOW:
	case EXCEPTION_ILLEGAL_INSTRUCTION:
	case EXCEPTION_INT_DIVIDE_BY_ZERO:
	case EXCEPTION_INT_OVERFLOW:
	case EXCEPTION_INVALID_DISPOSITION:
	case EXCEPTION_NONCONTINUABLE_EXCEPTION:
	case EXCEPTION_PRIV_INSTRUCTION:
	case EXCEPTION_SINGLE_STEP:
	case EXCEPTION_STACK_OVERFLOW:
		spdlog::critical("{} 0x{:X}", exception_definition, ExceptionInfo->ExceptionRecord->ExceptionCode);
		spdlog::critical("{}", exception_description);
		break;
	case EXCEPTION_ACCESS_VIOLATION:
	case EXCEPTION_IN_PAGE_ERROR:
		spdlog::critical("{} 0x{:X}", exception_definition, ExceptionInfo->ExceptionRecord->ExceptionCode);
		spdlog::critical("{}", exception_description);
		spdlog::critical("Address: 0x{:X} ({})", access_address, access_type);
		break;
	default:
		spdlog::critical("0x{:X}", ExceptionInfo->ExceptionRecord->ExceptionCode);
		spdlog::critical("{}", exception_description);
		break;
	}

	write_dump(ExceptionInfo);

	if (HWND const window = FindWindowW(L"Battlefield 4", nullptr); window) {
		ShowWindow(window, SW_FORCEMINIMIZE);
	}

	MessageBeep(MB_ICONERROR);
	MessageBoxA(0, fmt::format("{}\nAll debugging details have been saved to the Battlefield 4 folder", exception_description).c_str(), "Crash", MB_OK | MB_TOPMOST);

	return EXCEPTION_CONTINUE_SEARCH;
}
