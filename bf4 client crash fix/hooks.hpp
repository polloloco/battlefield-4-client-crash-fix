#pragma once
#include <cstdint>
#include <Windows.h>

namespace hook {

	void function(std::uintptr_t const a1, std::uintptr_t const a2, std::uintptr_t const a3);
	LONG unhandled_exception_filter(PEXCEPTION_POINTERS ExceptionInfo);


	inline decltype(&function) function_o{ reinterpret_cast<decltype(&function)>(0x14023C2D0) };
	inline decltype(&unhandled_exception_filter) unhandled_exception_filter_o{ reinterpret_cast<decltype(&unhandled_exception_filter)>(0x140006C00) };

}