#include "safe_handle.hpp"
#include <utility>

safe_handle::safe_handle()
	: m_handle{ INVALID_HANDLE_VALUE }
{
}

safe_handle::safe_handle(HANDLE handle)
	: m_handle{ handle }
{
}

safe_handle::safe_handle(safe_handle&& other) noexcept
	: m_handle{ std::exchange(other.m_handle, INVALID_HANDLE_VALUE) }
{
}

safe_handle& safe_handle::operator=(safe_handle&& other) noexcept
{
	close();
	m_handle = std::exchange(other.m_handle, INVALID_HANDLE_VALUE);
	return *this;
}

bool safe_handle::operator==(const safe_handle& other) const noexcept
{
	return m_handle == other.m_handle;
}

bool safe_handle::operator!=(const safe_handle& other) const noexcept
{
	return !(*this == other);
}

safe_handle::operator HANDLE() const noexcept
{
	return m_handle;
}

safe_handle::operator bool() const noexcept
{
	return is_valid();
}

bool safe_handle::is_valid() const noexcept
{
	return m_handle != INVALID_HANDLE_VALUE && m_handle != NULL;
}

void safe_handle::close() noexcept
{
	if (is_valid()) {
		CloseHandle(std::exchange(m_handle, INVALID_HANDLE_VALUE));
	}
}

void safe_handle::swap(safe_handle& other) noexcept
{
	std::swap(m_handle, other.m_handle);
}

safe_handle::~safe_handle()
{
	close();
}
