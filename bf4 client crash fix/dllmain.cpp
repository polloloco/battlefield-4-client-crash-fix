#include "dllmain.hpp"

#include "main.hpp"
#include "safe_handle.hpp"

BOOL DllMain(HINSTANCE hinstDLL, DWORD fdwReason, [[maybe_unused]] LPVOID lpReserved)
{
    if (fdwReason == DLL_PROCESS_ATTACH) {
#ifdef _DEBUG
        AllocConsole();
        SetConsoleCP(CP_UTF8);
        SetConsoleOutputCP(CP_UTF8);
#endif

        if (safe_handle const handle{ CreateThread(nullptr, 0, init_thread, hinstDLL, 0, nullptr) }; handle) {
            return TRUE;
        }
        return FALSE;
    }
    else if (fdwReason == DLL_PROCESS_DETACH) {
        eject();
#ifdef _DEBUG
        FreeConsole();
#endif
    }
    return TRUE;
}
