#pragma once

#include <Windows.h>

class safe_handle
{
	HANDLE m_handle;

public:
	safe_handle();
	explicit safe_handle(HANDLE handle);
	safe_handle(const safe_handle& other) = delete;
	safe_handle(safe_handle&& other) noexcept;

	safe_handle& operator=(const safe_handle& other) = delete;
	safe_handle& operator=(safe_handle&& other) noexcept;

	bool operator==(const safe_handle& other) const noexcept;
	bool operator!=(const safe_handle& other) const noexcept;

	operator HANDLE() const noexcept;
	operator bool() const noexcept;

	bool is_valid() const noexcept;
	void close() noexcept;

	void swap(safe_handle& other) noexcept;

	~safe_handle();
};
