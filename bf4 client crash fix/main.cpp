#include "main.hpp"
#include "hooks.hpp"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <MinHook.h>

#include <atomic>
#include <thread>
#include <chrono>

std::atomic_bool g_hooked{ false };

DWORD init_thread(LPVOID reserved)
{
    [[maybe_unused]] HINSTANCE dll = static_cast<HINSTANCE>(reserved);

#ifdef NDEBUG
    spdlog::default_logger_raw()->sinks().clear();
#endif

    spdlog::flush_on(spdlog::level::info);
    spdlog::default_logger_raw()->sinks().push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>("crashlog.txt"));

    if (bool const prev = g_hooked.exchange(true); prev == false) {
        MH_Initialize();

		MH_CreateHook(hook::unhandled_exception_filter_o, hook::unhandled_exception_filter, reinterpret_cast<LPVOID*>(&hook::unhandled_exception_filter_o));
        MH_CreateHook(hook::function_o, hook::function, reinterpret_cast<LPVOID*>(&hook::function_o));

        MH_EnableHook(MH_ALL_HOOKS);
    }

#ifdef _DEBUG
    spdlog::info("Initialized");
#endif

    return 0;
}

void eject()
{
    using namespace std::literals::chrono_literals;

#ifdef _DEBUG
    spdlog::info("Ejecting");
#endif

    if (bool const prev = g_hooked.exchange(false); prev == true) {
        MH_DisableHook(MH_ALL_HOOKS);

        std::this_thread::sleep_for(10ms);

        MH_Uninitialize();

#ifdef _DEBUG
        spdlog::info("Unhooked");
#endif
    }
}

#pragma region Code required to serve as a proxy dinput8.dll

#include <Unknwnbase.h>
#include <filesystem>
#include <array>

using DirectInput8Create_t = HRESULT(*)(HINSTANCE, DWORD, REFIID, LPVOID*, LPUNKNOWN);

HMODULE lazy_load_dinput8_dll() noexcept
{
	static HMODULE dinput8_dll = []() -> HMODULE {

		auto const get_original_dll_path = []() -> std::string {

			std::array<char, MAX_PATH> path;
			GetSystemDirectoryA(path.data(), static_cast<UINT>(path.size()));

			std::filesystem::path const system32(path.data());

			return (system32 / "dinput8.dll").string();
		};

		auto const dllpath(get_original_dll_path());

		if (auto const dll = LoadLibraryA(dllpath.c_str()); dll) {
			return dll;
		}
		else {
			std::terminate(); //couldnt load dinput8.dll, quit
		}

	}();

	return dinput8_dll;
}

DirectInput8Create_t lazy_load_dinput8create() noexcept
{
	static DirectInput8Create_t DirectInput8Create = []() -> DirectInput8Create_t {
		auto const dinput8_dll = lazy_load_dinput8_dll();

		if (auto const dll = reinterpret_cast<DirectInput8Create_t>(GetProcAddress(dinput8_dll, "DirectInput8Create")); dll) {
			return dll;
		}
		else {
			std::terminate(); //cant get DirectInput8Create from original dll, quit
		}

	}();
	return DirectInput8Create;
}

//provide the export so the executable thinks its loading the real dinput8.dll
extern "C" __declspec(dllexport) HRESULT WINAPI DirectInput8Create(HINSTANCE hinst, DWORD dwVersion, REFIID riidltf, LPVOID * ppvOut, LPUNKNOWN punkOuter)
{
	//return E_INVALIDARG;
	return lazy_load_dinput8create()(hinst, dwVersion, riidltf, ppvOut, punkOuter);
}

#pragma endregion
