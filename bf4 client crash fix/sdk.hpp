#pragma once
#include <cstdint>
#include <cstddef>

namespace fb {

	template <typename T>
	class EntityList;

	template <typename T>
	class EntityIterator
	{
	public:

		T* m_element{};

		T& operator*();
		void operator++();
		bool operator!=(const EntityIterator&);
		void operator=(T*);

	};

	template<typename T>
	inline T& EntityIterator<T>::operator*() { return *m_element; }

	template<typename T>
	inline void EntityIterator<T>::operator++()
	{
		if (!m_element || !m_element->m_flink) {
			m_element = nullptr;
			return;
		}

		m_element = m_element->m_flink;
	}

	template<typename T>
	inline bool EntityIterator<T>::operator!=(const EntityIterator& other) { return m_element != other.m_element; }

	template<typename T>
	inline void EntityIterator<T>::operator=(T* other) { m_element = other; }

	template <typename T, uint64_t Link>
	class Element
	{
	public:

		Element<T, Link>* m_flink;
		Element<T, Link>* m_blink;

		T* get() const noexcept;

		operator T* () const noexcept;

	};

	template<typename T, uint64_t Link>
	inline T* Element<T, Link>::get() const noexcept
	{
		std::intptr_t object = reinterpret_cast<std::intptr_t>(this);
		object -= Link;

		return reinterpret_cast<T*>(object);
	}

	template<typename T, uint64_t Link>
	inline Element<T, Link>::operator T* () const noexcept
	{
		return get();
	}

	template <typename T>
	class EntityList
	{
	public:

		using element_type = Element<T, T::iterablelink()>;
		using iterator = EntityIterator<element_type>;

		iterator m_first;

		iterator begin();
		iterator end();

		EntityList();

	};

	template<typename T>
	inline EntityList<T>::EntityList()
	{
		constexpr std::uintptr_t classinfo = T::static_typeinfo();
		if (std::uintptr_t const gameworld = *reinterpret_cast<std::uintptr_t*>(0x142668F90); gameworld) {
			m_first = reinterpret_cast<iterator(*)(std::uintptr_t const classinfo, std::uintptr_t const gameworld)>(0x1407CD5D0)(classinfo, gameworld);
		}
		else {
			m_first = iterator();
		}
	}

	template<typename T>
	inline EntityIterator<Element<T, T::iterablelink()>> EntityList<T>::begin() { return m_first; }

	template<typename T>
	inline EntityIterator<Element<T, T::iterablelink()>> EntityList<T>::end()
	{
		return {};
	}


	class ClientPlayerLogoEntity
	{
		char pad_0000[80]; //0x0000
	public:
		bool loaded; //0x0050
	private:
		char pad_0051[7]; //0x0051
	public:
		std::uint8_t const* buffer; //0x0058

	public:
		constexpr static std::uintptr_t static_typeinfo() noexcept {
			return 0x142BB7EB0;
		}

		constexpr static std::size_t iterablelink() noexcept {
			return 0x38;
		};
	};
}